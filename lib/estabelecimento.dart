import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cliki/json.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';


import 'package:url_launcher/url_launcher.dart';



class Estabelecimento extends StatefulWidget { 
  Estabelecimento({@required this.estabelecimento});
 final Autogenerated estabelecimento;
  @override
  _EstabelecimentoState createState() => _EstabelecimentoState();
}

class _EstabelecimentoState extends State<Estabelecimento> {
   String urlImagemTeste = "https://www.cliki.com.br/pesquisa/fotos/.padrao/banner3_1.jpg";
   String baseUrl = "https://www.cliki.com.br";
   TextStyle textStyleNavegacao = TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: 15);
   TextStyle textStyleTitulo = TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: 25);
   TextStyle textStyleContato = TextStyle(fontWeight: FontWeight.bold,color:Colors.blue[900],decoration: TextDecoration.underline,fontSize: 12);
   AutoScrollController controller;
   Widget paddingSecoes = Padding(padding: EdgeInsets.only(top: 25),);
   final scrollDirection = Axis.vertical;
   
   int  _current = 0;
  Iterable<E> mapIndexed<E, T>(
    Iterable<T> items, E Function(int index, T item) f) sync* {
  var index = 0;

  for (final item in items) {
    yield f(index, item);
    index = index + 1;
  }
}

  _launchCaller(String contato) async {
    if(contato.contains("https")){
      if (await canLaunch(contato)) {
       await launch(contato);
      } else {
        throw 'Could not launch $contato';
      } 
    }else{
    if(contato.contains("@")){
      if (await canLaunch("mailto:"+contato+"?subject=t&body=t")) {
       await launch("mailto:"+contato);
      } else {
        throw 'Could not launch $contato';
      } 
    }else{
    String url = "tel:"+ contato.trim().replaceAll('(','').replaceAll(')','');
    if (await canLaunch(url)) {
       await launch(url);
    } else {
      throw 'Could not launch $url';
    } 
    }
    }
    
      
}
 Future _scrollToIndex(int index) async {
    

    await controller.scrollToIndex(index, preferPosition: AutoScrollPosition.middle);
    controller.highlight(index);
  }

 @override
  void initState()  {        // TODO: implement initState
    super.initState();  
     controller = AutoScrollController(
      viewportBoundaryGetter: () => Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),
      axis: scrollDirection
    );
    print( "ESTABELCIMENTO " + this.widget.estabelecimento.toString());
    
  }
  
  @override
  Widget build(BuildContext context) {    
    
        return Scaffold(
       appBar:null,
       body: SafeArea(
         top: false,
         left: false,
         right: false,
         child: CustomScrollView(
           controller: controller,
           scrollDirection: scrollDirection,
           slivers: <Widget>[
             SliverAppBar(
               leading:InkWell(
                 onTap: (){
                   Navigator.of(context).maybePop();
                 },
                 child:Icon(Icons.arrow_back,color: Colors.white,size: 30,)  ,
               ) ,
               title:InkWell(
                 onTap: (){
                   _launchCaller(this.widget.estabelecimento.telefones);
                 },
                 child:Text(this.widget.estabelecimento.telefones),
               )
                ,
              actions: <Widget>[
                ],
               flexibleSpace:SizedBox(
      width: double.infinity,
      height: double.infinity,
      child: Stack(
                                children: <Widget>[
                            Center(
                              child:Stack(
                                children: <Widget>[                                  
                                  CarouselSlider(
                                autoPlay: true,
                                pauseAutoPlayOnTouch: Duration(seconds: 10),
                                height: 350,
                                viewportFraction: 1.0,
                                onPageChanged: (index){
                                  setState(() {
                                    _current = index;
                                  });
                                },
                                items:[
                                 if(this.widget.estabelecimento.arquivos.banner.length > 0) ...this.widget.estabelecimento.arquivos.banner.map((img){
                                    return  Container(
                                          width: MediaQuery.of(context).size.width,
                                          child: CachedNetworkImage(
                                            fit: BoxFit.cover,
                                            imageUrl: baseUrl + img,
                                            placeholder: (context, url) => Center(
                                                child: CircularProgressIndicator()
                                            ),
                                            errorWidget: (context, url, error) => new Icon(Icons.error),
                                          )
                                      );
                                  })
                                 ,
                                 if(this.widget.estabelecimento.arquivos.banner.length == 0) Container(
                                          width: MediaQuery.of(context).size.width,
                                          child: CachedNetworkImage(
                                            fit: BoxFit.cover,
                                            imageUrl:"https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Item_sem_imagem.svg/498px-Item_sem_imagem.svg.png",
                                            placeholder: (context, url) => Center(
                                                child: CircularProgressIndicator()
                                            ),
                                            errorWidget: (context, url, error) => new Icon(Icons.error),
                                          )
                                      )
                                      
                                ]
                              ),
                               
                                ],
                              )
                              
                                
              ),
             (this.widget.estabelecimento.arquivos.logotipo != null) ? Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: EdgeInsets.only(top: 30,right: 20),
                  child:SizedBox(
                   height: 80,
                   width: 80,
                   child:ClipOval(                     
                     child: Container(
                       decoration: BoxDecoration(
                         color: Colors.white
                       ),
                       child:CachedNetworkImage(                           
                           fit: BoxFit.contain,
                           imageUrl: baseUrl + this.widget.estabelecimento.arquivos.logotipo,
                           placeholder: (context,url)=>SizedBox(width: 50,height: 50,child: CircularProgressIndicator(),),
                           errorWidget: (context,url,error)=>Icon(Icons.error),
                         ) ,
                     )
                        ,
                   )
                    ,
                 ) ,
                ),
              ) : Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: EdgeInsets.only(top: 30,right: 20),
                  child:SizedBox(
                   height: 80,
                   width: 80,
                   child:ClipOval(                     
                     child: Container(
                       decoration: BoxDecoration(
                         color: Colors.white
                       ),
                       child:CachedNetworkImage(                           
                           fit: BoxFit.contain,
                           imageUrl: "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Item_sem_imagem.svg/498px-Item_sem_imagem.svg.png",
                           placeholder: (context,url)=>SizedBox(width: 50,height: 50,child: CircularProgressIndicator(),),
                           errorWidget: (context,url,error)=>Icon(Icons.error),
                         ) ,
                     )
                        ,
                   )
                    ,
                 ) ,
                ),
              )
          ],
          ),
    ),
              expandedHeight:  300,
             ), 
             SliverList(
               delegate: SliverChildBuilderDelegate(
                 (context,index)=> Padding(padding: EdgeInsets.all(0),
                 child: Column(
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: <Widget>[
                     Container(
                       decoration: BoxDecoration(
                         gradient: LinearGradient(
                           begin: Alignment.topLeft,
                           end: Alignment.bottomRight,
                           colors: [Colors.white,Colors.grey[350]],
                           tileMode: TileMode.repeated
                         )
                       ),
                       child:Padding(
                         padding: EdgeInsets.only(top: 20,bottom: 20),
                         child: Row(
                         mainAxisAlignment: MainAxisAlignment.spaceAround,
                         children: <Widget>[
                           InkWell(
                             onTap: (){
                               _scrollToIndex(1);
                             },
                             child:Text("SOBRE",style: textStyleNavegacao,) ,
                           )
                           ,
                           InkWell(
                             onTap: (){_scrollToIndex(2);},
                             child:Text("SERVIÇOS",style: textStyleNavegacao), 
                           ),
                           InkWell(
                             onTap: (){_scrollToIndex(3);},
                             child:Text("GALERIA",style: textStyleNavegacao,) ,
                           ),
                           InkWell(
                             onTap: (){_scrollToIndex(4);},
                             child: Text("CONTATO",style: textStyleNavegacao,),
                           )
                           
                         ],
                       ),
                       ),

                     ),
                     Padding(padding: EdgeInsets.all(5),),
                      Center(                      
                       child: Text( this.widget.estabelecimento.segmento),
                     ),
                     paddingSecoes,
                    
                     Padding(padding: EdgeInsets.all(5),),
                       AutoScrollTag(
                         key: ValueKey(1),
                         controller: controller,
                         index: 1,
                         child:Center(
                         child: Text("SOBRE",style: textStyleTitulo,),
                       ) ,
                       highlightColor: Colors.black.withOpacity(0.1),
                       )
                       ,
                       paddingSecoes,
                       Padding(
                         padding: EdgeInsets.only(left: 15,right: 15),
                         child: Center(
                           child: Align(
                             alignment: Alignment.center,
                             child:Text(this.widget.estabelecimento.descricao.completa) ,
                           )
                           ,
                            ),
                            ),
                        paddingSecoes,
                     AutoScrollTag(
                         key: ValueKey(2),
                         controller: controller,
                         index: 2,
                         child:Center(
                         child: Text("SERVIÇOS",style: textStyleTitulo,),
                       ) ,
                       highlightColor: Colors.black.withOpacity(0.1),
                       ),
                        if( this.widget.estabelecimento.servicos.length > 0) ...this.widget.estabelecimento.servicos.map<Widget>((s){
                        return Column(
                          children: <Widget>[
                          Padding(
                         padding: EdgeInsets.only(left: 15,right: 15,bottom: 10,top: 5),
                         child: Center(
                           child: Text(s.titulo,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                            ),
                            ),
                          if(s.imagem != "") CachedNetworkImage(
                                                        height: MediaQuery.of(context).size.width,
                                                        width: MediaQuery.of(context).size.width,
                                                        fit: BoxFit.cover,
                                                        imageUrl: baseUrl+s.imagem,
                                                        placeholder: (context,url)=>SizedBox(width: 50,height: 50,child: CircularProgressIndicator(),),
                                                        errorWidget: (context,url,error)=>Icon(Icons.error),
                                                      )
                          ],
                        ) 
                        
                         ;
                       }).toList(),
                       paddingSecoes,
                     
                       AutoScrollTag(
                         key: ValueKey(3),
                         controller: controller,
                         index: 3,
                         child: Center(
                         child: Text("GALERIA",style: textStyleTitulo,),
                         )  ,
                         highlightColor: Colors.black.withOpacity(0.1),
                       )
                       ,
                       GridView.count(
                       shrinkWrap: true,
                       physics: NeverScrollableScrollPhysics(),
                       crossAxisCount: 2,
                       children: <Widget>[
                         ...this.widget.estabelecimento.arquivos.galeria.map((imagem){
                           return InkWell(
                           onTap: (){
                           Navigator.of(context).push(
                                  new PageRouteBuilder(
                                      opaque: false,
                                      barrierDismissible:true,
                                      pageBuilder: (BuildContext context, _, __) {
                                          return Container(
                                              color: Colors.black.withOpacity(0.5),
                                              width: MediaQuery.of(context).size.width/2,
                                              height: MediaQuery.of(context).size.width/2,
                                              child: Hero(
                                                  tag: baseUrl+imagem,
                                                  child:CachedNetworkImage(
                                                        height: MediaQuery.of(context).size.width/2,
                                                        width: MediaQuery.of(context).size.width/2,
                                                        fit: BoxFit.contain,
                                                        imageUrl: baseUrl+imagem,
                                                        placeholder: (context,url)=>SizedBox(width: 50,height: 50,child: CircularProgressIndicator(),),
                                                        errorWidget: (context,url,error)=>Icon(Icons.error),
                                                      ),
                                              ),
                                          );
                                      }
                                  )
                              );
                           },
                           child:Hero(
                             tag: baseUrl+imagem,
                             child:CachedNetworkImage(
                           height: MediaQuery.of(context).size.width/2,
                           width: MediaQuery.of(context).size.width/2,
                           fit: BoxFit.cover,
                           imageUrl: baseUrl+imagem,
                           placeholder: (context,url)=>SizedBox(width: 50,height: 50,child: CircularProgressIndicator(),),
                           errorWidget: (context,url,error)=>Icon(Icons.error),
                         ) ,
                           )
                            ,
                           )
                            ;
                         })
                         ] ,
                     ),
                     paddingSecoes,
                     AutoScrollTag(
                       key: ValueKey(4),
                       controller: controller,
                       index: 4,
                       child:  Center(child:Text("Contato",style:textStyleTitulo) ,),
                       highlightColor: Colors.black.withOpacity(0.1),
                     )
                    ,
                     paddingSecoes,

                     Row(
                       mainAxisAlignment: MainAxisAlignment.spaceAround,
                       children: <Widget>[

                      if(this.widget.estabelecimento.telefones != '') InkWell(
                        onTap: (){
                        _launchCaller(this.widget.estabelecimento.telefones);
                       },
                        child: Row(
                           mainAxisAlignment: MainAxisAlignment.center,
                           children: <Widget>[
                             ClipOval(
                               child: Container(
                                 decoration: BoxDecoration(color: Colors.blueAccent),
                                 height: 30,
                                 width: 30,
                                 child: Icon(Icons.phone_in_talk , color:Colors.white),
                               ),
                             ),
                             Padding(padding: EdgeInsets.only(left: 15)),
                             Column(
                               mainAxisAlignment: MainAxisAlignment.center,
                               children: <Widget>[
                                   Text(this.widget.estabelecimento.telefones,style:textStyleContato),
                                  
                                  ],
                             )
                            
                           ],
                         ),
                      )   ,
                         if(this.widget.estabelecimento.contatos.email != '') InkWell(
                           onTap: (){
                             _launchCaller(this.widget.estabelecimento.contatos.email);
                           },
                           child: Row(
                           mainAxisAlignment: MainAxisAlignment.center,
                           children: <Widget>[
                             ClipOval(
                               child: Container(
                                 decoration: BoxDecoration(color: Colors.blueAccent),
                                 height: 30,
                                 width: 30,
                                 child: Icon(Icons.mail_outline , color:Colors.white),
                               ),
                             ),
                             Padding(padding: EdgeInsets.only(left: 15)),
                             Column(
                               mainAxisAlignment: MainAxisAlignment.center,
                               children: <Widget>[
                                   Text(this.widget.estabelecimento.contatos.email,style:textStyleContato),
                                  
                                  ],
                             )
                            
                           ],
                         ),
                         )
                          

                       ],
                       ),//Fim da row de contatos
                       paddingSecoes,
                       Center(child:Text("Endereço",style:textStyleTitulo) ,),
                       paddingSecoes,
                       Align(
                         alignment: Alignment.center,
                         child:Padding(
                         padding: EdgeInsets.only(left: 15,right: 15),
                         child: Align(
                       alignment: Alignment.center,
                       child: Text(this.widget.estabelecimento.endereco[0].logradouro + " " +
                                   this.widget.estabelecimento.endereco[0].numero + " " +
                                   this.widget.estabelecimento.endereco[0].bairro + " " +
                                   this.widget.estabelecimento.endereco[0].cidade + " " + "Cep:" +
                                   this.widget.estabelecimento.endereco[0].cEP  ,style: TextStyle(),),
                      ),
                       ) ,
                       )
                       ,
                       paddingSecoes
                       ,
                       Align(
                         alignment: Alignment.center,
                         child:InkWell(
                         onTap: (){
                           _launchCaller("https://www.google.com/maps/search/?api=1&query="+
                                   this.widget.estabelecimento.endereco[0].logradouro.replaceAll(" ","") +
                                   this.widget.estabelecimento.endereco[0].numero.replaceAll(" ","")+
                                   this.widget.estabelecimento.endereco[0].bairro.replaceAll(" ","")+
                                   this.widget.estabelecimento.endereco[0].cidade.replaceAll(" ",""));
                         },
                         child:Center(child: Container(
                        width: MediaQuery.of(context).size.width * 0.5,
                        height: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          color: Theme.of(context).primaryColor
                        ),
                        child: Center(child: Text("VER NO GOOGLE MAPS",style: TextStyle(color: Colors.white,fontSize: 11),)                        ,
                      )
                      ,),) ,
                       ) ,
                       )
                       
                       ,
                       paddingSecoes,
                       Center(
                         child:Row(
                           mainAxisAlignment: MainAxisAlignment.center,
                           children: <Widget>[
                             if(this.widget.estabelecimento.contatos.facebook != null) InkWell(
                               onTap: (){
                                 _launchCaller(this.widget.estabelecimento.contatos.facebook);
                               },
                               child: ClipOval(
                               child: Container(
                                 decoration: BoxDecoration(color: Colors.blueAccent),
                                 height: 35,
                                 width: 35,
                                 child: Icon(FontAwesomeIcons.facebook, color:Colors.white),
                               ),
                             ) ,
                             )
                            ,
                             Padding(padding: EdgeInsets.only(left: 5)),
                             if(this.widget.estabelecimento.contatos.linkedin != null) InkWell(
                               onTap: (){
                                 _launchCaller(this.widget.estabelecimento.contatos.linkedin);
                              },
                              child: ClipOval(
                               child: Container(
                                 decoration: BoxDecoration(color: Colors.blue),
                                 height: 35,
                                 width: 35,
                                 child: Icon(FontAwesomeIcons.linkedinIn, color:Colors.white),
                               ),
                             ),
                             ),
                             Padding(padding: EdgeInsets.only(left: 5)),
                             if(this.widget.estabelecimento.contatos.twitter != null) InkWell(
                               onTap: (){
                                 _launchCaller(this.widget.estabelecimento.contatos.twitter);
                               },
                               child: ClipOval(
                               child: Container(
                                 decoration: BoxDecoration(color: Colors.blue),
                                 height: 35,
                                 width: 35,
                                 child: Icon(FontAwesomeIcons.twitter , color:Colors.white),
                               ),
                             ),
                             ),
                             Padding(padding: EdgeInsets.only(left: 5)),
                             if(this.widget.estabelecimento.contatos.instagram != null) InkWell(
                               onTap: (){
                                 _launchCaller(this.widget.estabelecimento.contatos.instagram);
                               },
                               child: ClipOval(
                               child: Container(
                                 decoration: BoxDecoration(color: Colors.blue[900]),
                                 height: 35,
                                 width: 35,
                                 child: Icon(FontAwesomeIcons.instagram , color:Colors.white),
                               ),
                             ) ,
                             ),
                             Padding(padding: EdgeInsets.only(left: 5)),
                             if(this.widget.estabelecimento.contatos.youtube != null) InkWell(
                               onTap: (){
                                 _launchCaller(this.widget.estabelecimento.contatos.youtube);
                               },
                               child: ClipOval(
                               child: Container(
                                 decoration: BoxDecoration(color: Colors.redAccent),
                                 height: 35,
                                 width: 35,
                                 child: Icon(FontAwesomeIcons.youtube, color:Colors.white),
                               ),
                             ) ,
                             ),
                             
                           ],
                           
                                                    ) ,
                       ),
                      paddingSecoes,
                      
                      paddingSecoes,
                       Container(
                          height: 50,
                          width: MediaQuery.of(context).size.width,                          
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Image.asset("assets/images/branco.png",height: 80,width: 80,),
                              InkWell(
                                onTap: (){
                                    _launchCaller("https://www.cliki.com.br/cadastro_empresa.php");
                                },
                                child: Container(
                                height: 30,
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.white,),
                                  borderRadius: BorderRadius.all(Radius.circular(5))
                                ),
                                child: Padding(
                                  padding: EdgeInsets.all(5),
                                  child:Center(child: Text("Cadastrar minha Empresa",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 12),),) ,
                                )
                                ,
                              ),
                              )
                              
                            ],
                          ),
                          color: Theme.of(context).primaryColor,
                        )

                      
                      


                     ],
                 ),),
                 childCount: 1,
               ),
             )           
           ],
           

         ),
       )
  );
  

}}