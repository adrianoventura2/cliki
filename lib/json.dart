class Autogenerated {
  int codigo;
  String segmento;
  String nome;
  Descricao descricao;
  String telefones;
  bool recomendado;
  List<Endereco> endereco;
  Contatos contatos;
  List<Servicos> servicos;
  Arquivos arquivos;

  Autogenerated(
      {this.codigo,
      this.segmento,
      this.nome,
      this.descricao,
      this.telefones,
      this.recomendado,
      this.endereco,
      this.contatos,
      this.servicos,
      this.arquivos});

  Autogenerated.fromJson(Map<String, dynamic> json) {
    codigo = json['Codigo'];
    segmento = json['Segmento'];
    nome = json['Nome'];
    descricao = json['Descricao'] != null
        ? new Descricao.fromJson(json['Descricao'])
        : null;
    telefones = json['Telefones'];
    recomendado = json['Recomendado'];
    if (json['Endereco'] != null) {
      endereco = new List<Endereco>();
      json['Endereco'].forEach((v) {
        endereco.add(new Endereco.fromJson(v));
      });
    }
    contatos = json['Contatos'] != null
        ? new Contatos.fromJson(json['Contatos'])
        : null;
    if (json['Servicos'] != null) {
      servicos = new List<Servicos>();
      json['Servicos'].forEach((v) {
        servicos.add(new Servicos.fromJson(v));
      });
    }
    arquivos = json['Arquivos'] != null
        ? new Arquivos.fromJson(json['Arquivos'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Codigo'] = this.codigo;
    data['Segmento'] = this.segmento;
    data['Nome'] = this.nome;
    if (this.descricao != null) {
      data['Descricao'] = this.descricao.toJson();
    }
    data['Telefones'] = this.telefones;
    data['Recomendado'] = this.recomendado;
    if (this.endereco != null) {
      data['Endereco'] = this.endereco.map((v) => v.toJson()).toList();
    }
    if (this.contatos != null) {
      data['Contatos'] = this.contatos.toJson();
    }
    if (this.servicos != null) {
      data['Servicos'] = this.servicos.map((v) => v.toJson()).toList();
    }
    if (this.arquivos != null) {
      data['Arquivos'] = this.arquivos.toJson();
    }
    return data;
  }
}

class Descricao {
  String reduzida;
  String completa;

  Descricao({this.reduzida, this.completa});

  Descricao.fromJson(Map<String, dynamic> json) {
    reduzida = json['Reduzida'];
    completa = json['Completa'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Reduzida'] = this.reduzida;
    data['Completa'] = this.completa;
    return data;
  }
}

class Endereco {
  String logradouro;
  String numero;
  String complemento;
  String bairro;
  String cidade;
  String cEP;
  Coordenadas coordenadas;

  Endereco(
      {this.logradouro,
      this.numero,
      this.complemento,
      this.bairro,
      this.cidade,
      this.cEP,
      this.coordenadas});

  Endereco.fromJson(Map<String, dynamic> json) {
    logradouro = json['Logradouro'];
    numero = json['Numero'];
    complemento = json['Complemento'];
    bairro = json['Bairro'];
    cidade = json['Cidade'];
    cEP = json['CEP'];
    coordenadas = json['Coordenadas'] != null
        ? new Coordenadas.fromJson(json['Coordenadas'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Logradouro'] = this.logradouro;
    data['Numero'] = this.numero;
    data['Complemento'] = this.complemento;
    data['Bairro'] = this.bairro;
    data['Cidade'] = this.cidade;
    data['CEP'] = this.cEP;
    if (this.coordenadas != null) {
      data['Coordenadas'] = this.coordenadas.toJson();
    }
    return data;
  }
}

class Coordenadas {
  String lat;
  String lng;

  Coordenadas({this.lat, this.lng});

  Coordenadas.fromJson(Map<String, dynamic> json) {
    lat = json['Lat'];
    lng = json['Lng'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Lat'] = this.lat;
    data['Lng'] = this.lng;
    return data;
  }
}

class Contatos {
  String email;
  String site;
  String facebook;
  String instagram;
  String twitter;
  String linkedin;
  String youtube;

  Contatos(
      {this.email,
      this.site,
      this.facebook,
      this.instagram,
      this.twitter,
      this.linkedin,
      this.youtube});

  Contatos.fromJson(Map<String, dynamic> json) {
    email = json['Email'];
    site = json['Site'];
    facebook = json['Facebook'];
    instagram = json['Instagram'];
    twitter = json['Twitter'];
    linkedin = json['Linkedin'];
    youtube = json['Youtube'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Email'] = this.email;
    data['Site'] = this.site;
    data['Facebook'] = this.facebook;
    data['Instagram'] = this.instagram;
    data['Twitter'] = this.twitter;
    data['Linkedin'] = this.linkedin;
    data['Youtube'] = this.youtube;
    return data;
  }
}

class Servicos {
  int codigo;
  String titulo;
  String descricao;
  String imagem;

  Servicos({this.codigo, this.titulo, this.descricao, this.imagem});

  Servicos.fromJson(Map<String, dynamic> json) {
    codigo = json['Codigo'];
    titulo = json['Titulo'];
    descricao = json['Descricao'];
    imagem = json['Imagem'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Codigo'] = this.codigo;
    data['Titulo'] = this.titulo;
    data['Descricao'] = this.descricao;
    data['Imagem'] = this.imagem;
    return data;
  }
}

class Arquivos {
  String logotipo;
  List<String> galeria;
  List<String> banner;

  Arquivos({this.logotipo, this.galeria, this.banner});

  Arquivos.fromJson(Map<String, dynamic> json) {
    logotipo = json['Logotipo'];
    galeria = json['Galeria'].cast<String>();
    banner = json['Banner'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Logotipo'] = this.logotipo;
    data['Galeria'] = this.galeria;
    data['Banner'] = this.banner;
    return data;
  }
}