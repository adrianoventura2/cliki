
import 'package:cliki/estabelecimento.dart';
import 'package:cliki/resultadopesquisa.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cliki',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(        
        primaryColor: Color(0xffF76D00)	
      ),
      initialRoute: '/',
      onGenerateRoute: (RouteSettings settings) {
              print('build route for ${settings.name}');
              if(settings.arguments != null) print(settings.arguments);
              var routes = <String, WidgetBuilder>{
                "/": (ctx) => MyHomePage(title: 'Cliki'),
                "/ResultadoPesquisa": (ctx) => ResultadoPesquisa(pesquisar: settings.arguments),
                "/Estabelecimento":(ctx)=>Estabelecimento(estabelecimento:settings.arguments)
              };
              WidgetBuilder builder = routes[settings.name];
              return MaterialPageRoute(builder: (ctx) => builder(ctx));
            },
     
   
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {  
  Color corFundo  = Color(0xffF76D00)	;
  Color corLetras = Colors.white;
  bool pesquisando = false;
  String pesquisar;
 _launchCaller(String url) async {    
    if (await canLaunch(url)) {
       await launch(url);
    } else {
      throw 'Could not launch $url';
    }   
}
  

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: null,      
      body: AnimatedContainer(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        duration: Duration(seconds: 1),
        color: corFundo,
        child: Center(
        
        child: Column(          
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(padding: EdgeInsets.only(top: 100),),
            Align(
              alignment: Alignment.topCenter ,
              child: Image.asset('assets/images/branco.png',height: 150,width: 250,)             
            ),
            AnimatedContainer(
              duration: Duration(seconds: 1),             
              margin: EdgeInsets.all(20),
              child: Row(
                mainAxisSize: MainAxisSize.max,               
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                        Expanded(
                          child: TextField(
                                  decoration: InputDecoration(
                                    hintText: "Pesquisar",
                                    hintStyle: TextStyle(color: corLetras),
                                    border: InputBorder.none,                               
                                  
                                  ),
                                  style: TextStyle(color: corLetras),
                                  onChanged: (s){
                                    setState(() {
                                      pesquisar = s;
                                    });
                                    
                                  },
                                  onSubmitted: (string){
                                      Navigator.of(context).pushNamed('/ResultadoPesquisa',arguments:pesquisar);
                                      print("Funcao submite");
                                      setState(() {
                                        corFundo  = Colors.deepOrange;
                                        corLetras = Colors.white;
                                        pesquisando = true;
                                      });
                                  },
                                 ),
                           
                        ), 
                        InkWell(
                          onTap: ()async{
                            Navigator.of(context).pushNamed('/ResultadoPesquisa',arguments:pesquisar);
                            print("Funcao pesquisar");
                            setState(() {
                              corFundo  = Theme.of(context).primaryColor;
                              corLetras = Colors.white;
                              pesquisando = true;
                            });
                           
                          },

                          child:Icon(Icons.search,color:corLetras,size:25) , 
                        )
                                               
                   
                ],
              ),
               
              decoration: BoxDecoration(
                 color: corFundo,
                border: Border(
                  bottom: BorderSide(color:corLetras,style: BorderStyle.solid)
                )
              ),
            ),
            Expanded(
              child:Padding(
                padding: EdgeInsets.only(bottom: 30),
                child:  Align(
              alignment: Alignment.bottomCenter,
              child: InkWell(
                onTap: (){
                  print("FUNÇÃO LANCAR URL ATIVADA");
                  _launchCaller("http://www.cliki.com.br/cadastro_empresa.php");
                },
                child:SizedBox(
                width: MediaQuery.of(context).size.width * 0.7,
                height: 50,
                child: Container(
                  child:Center(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset('assets/images/icone-branco.png',height: 30,width: 30,),
                      Padding(padding: EdgeInsets.only(left: 10),),
                      Text("Cadastrar Minha Empresa",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),)
                    ],
                  )
                  ,
                ),
                decoration: BoxDecoration(
                  border: Border.all(color:Colors.white)
                ),
                ),
              ) ,
              )
              ,
            ) ,
            ),
              )
           
          ],
        ),
      ) ,
      )
     ,     
    );
  }
}
